package com.kapital.a20220502_davidvenadero_nycschools.di

import com.kapital.a20220502_davidvenadero_nycschools.data.repository.SchoolRepository
import com.kapital.a20220502_davidvenadero_nycschools.mock.FakeSchoolRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object MockNetworkModule {
    @Singleton
    @Provides
    fun provideRepository(): SchoolRepository =
        FakeSchoolRepository()
}