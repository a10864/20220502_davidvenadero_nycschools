package com.kapital.a20220502_davidvenadero_nycschools.ui

import androidx.navigation.Navigation
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import com.adevinta.android.barista.assertion.BaristaVisibilityAssertions.assertDisplayed
import com.kapital.a20220502_davidvenadero_nycschools.R
import com.kapital.a20220502_davidvenadero_nycschools.di.NetworkModule
import com.kapital.a20220502_davidvenadero_nycschools.launchFragmentInHiltContainer
import com.kapital.a20220502_davidvenadero_nycschools.ui.school.SchoolFragment
import com.kapital.a20220502_davidvenadero_nycschools.ui.school.SchoolViewHolder
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@UninstallModules(
    NetworkModule::class
)
@HiltAndroidTest
class SchoolFragmentTest {
    private val navController = TestNavHostController(
        ApplicationProvider.getApplicationContext()
    )

    @get:Rule
    var hiltRule = HiltAndroidRule(this)


    @Before
    fun setup(){
        hiltRule.inject()
    }

    @Before
    fun launchFragment(){
        launchFragmentInHiltContainer<SchoolFragment> {
            navController.setGraph(R.navigation.nav_graph)
            navController.setCurrentDestination(R.id.schoolScoresFragment)
            Navigation.setViewNavController(this.requireView(),navController)
            SchoolFragment()
        }
    }
    @Test
    fun displayRecyclerView(){
        assertDisplayed(R.id.rv_school)
    }
    @Test
    fun displayProvideElement(){
        assertDisplayed("Clinton School Writers & Artists, M.S. 260")
    }

    @Test
    fun clickOnButtOnAndNavigateToNextFragment(){
        Espresso.onView(withId(R.id.rv_school)).perform(
            RecyclerViewActions.actionOnItem<SchoolViewHolder>(
                hasDescendant(withId(R.id.btn_action)),click())
            )
        assertEquals(navController.currentDestination?.id,R.id.schoolScoresFragment)

    }

}