package com.kapital.a20220502_davidvenadero_nycschools.ui

import com.adevinta.android.barista.assertion.BaristaVisibilityAssertions.assertDisplayed
import com.kapital.a20220502_davidvenadero_nycschools.R
import com.kapital.a20220502_davidvenadero_nycschools.di.NetworkModule
import com.kapital.a20220502_davidvenadero_nycschools.launchFragmentInHiltContainer
import com.kapital.a20220502_davidvenadero_nycschools.ui.schoolscores.SchoolScoresFragment
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@UninstallModules(
    NetworkModule::class
)
@HiltAndroidTest
class SchoolScoresFragmentTest {
    @get:Rule
    var hiltRule = HiltAndroidRule(this)
    @Before
    fun setup(){
        hiltRule.inject()
    }

    @Before
    fun launchFragment(){
        launchFragmentInHiltContainer<SchoolScoresFragment> {
            SchoolScoresFragment()
        }
    }
    @Test
    fun validateShowFragment(){
        assertDisplayed(R.id.txt_title)
    }
}