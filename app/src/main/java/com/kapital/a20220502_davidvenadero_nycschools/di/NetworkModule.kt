package com.kapital.a20220502_davidvenadero_nycschools.di

import com.kapital.a20220502_davidvenadero_nycschools.data.remote.SchoolService
import com.kapital.a20220502_davidvenadero_nycschools.data.repository.DefaultSchoolRepository
import com.kapital.a20220502_davidvenadero_nycschools.data.repository.SchoolRepository
import com.kapital.a20220502_davidvenadero_nycschools.utils.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit = Retrofit.Builder()
        .baseUrl(Constants.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    @Singleton
    @Provides
    fun provideService(retrofit: Retrofit): SchoolService =
        retrofit.create(SchoolService::class.java)

    @Singleton
    @Provides
    fun provideRepository(schoolService: SchoolService):SchoolRepository=
        DefaultSchoolRepository(schoolService)


}