package com.kapital.a20220502_davidvenadero_nycschools.data.remote

import retrofit2.Response
import retrofit2.http.GET

interface SchoolService {
    @GET("s3k6-pzi2.json")
    suspend fun getAllSchools(): Response<List<SchoolRemote>>

    @GET("f9bf-2cp4.json")
    suspend fun getAllSchoolScores(): Response<List<SchoolSatRemote>>
}