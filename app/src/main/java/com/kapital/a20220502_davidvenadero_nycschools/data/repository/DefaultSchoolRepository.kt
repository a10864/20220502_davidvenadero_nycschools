package com.kapital.a20220502_davidvenadero_nycschools.data.repository

import com.kapital.a20220502_davidvenadero_nycschools.data.remote.SchoolService
import javax.inject.Inject

class DefaultSchoolRepository @Inject constructor(
    private val schoolService: SchoolService
):SchoolRepository {
    override suspend fun getAllSchools() =   schoolService.getAllSchools()
    override suspend fun getAllSchoolScore() = schoolService.getAllSchoolScores()
}