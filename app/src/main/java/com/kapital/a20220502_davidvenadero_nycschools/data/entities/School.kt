package com.kapital.a20220502_davidvenadero_nycschools.data.entities

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class School(
    val dbn: String,
    val name: String,
    val location: String,
    val description: String
) : Parcelable