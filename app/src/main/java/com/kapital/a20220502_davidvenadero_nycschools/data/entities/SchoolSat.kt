package com.kapital.a20220502_davidvenadero_nycschools.data.entities

data class SchoolSat(
    val dbn: String = "",
    val schoolName: String = "",
    val numSat: String = "",
    val scoreMat: String = "",
    val scoreWrite: String = "",
    val scoreReading: String = ""
)