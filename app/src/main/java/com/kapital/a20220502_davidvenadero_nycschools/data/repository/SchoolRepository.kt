package com.kapital.a20220502_davidvenadero_nycschools.data.repository

import com.kapital.a20220502_davidvenadero_nycschools.data.remote.SchoolRemote
import com.kapital.a20220502_davidvenadero_nycschools.data.remote.SchoolSatRemote
import retrofit2.Response

interface SchoolRepository {
    suspend fun getAllSchools(): Response<List<SchoolRemote>>
    suspend fun getAllSchoolScore() :Response<List<SchoolSatRemote>>
}