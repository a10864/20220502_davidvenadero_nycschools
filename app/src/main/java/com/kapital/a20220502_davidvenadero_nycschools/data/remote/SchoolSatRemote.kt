package com.kapital.a20220502_davidvenadero_nycschools.data.remote

import com.kapital.a20220502_davidvenadero_nycschools.data.entities.SchoolSat

data class SchoolSatRemote(
    val dbn: String,
    val num_of_sat_test_takers: String,
    val sat_critical_reading_avg_score: String,
    val sat_math_avg_score: String,
    val sat_writing_avg_score: String,
    val school_name: String
){
    companion object {

        fun SchoolSatRemote.toSchoolSat() = SchoolSat(
            dbn = dbn,
            schoolName = school_name,
            numSat = num_of_sat_test_takers,
            scoreMat = sat_math_avg_score,
            scoreWrite = sat_writing_avg_score,
            scoreReading = sat_critical_reading_avg_score
        )
    }
}
