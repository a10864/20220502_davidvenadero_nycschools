package com.kapital.a20220502_davidvenadero_nycschools.app

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class Application:Application() {
}