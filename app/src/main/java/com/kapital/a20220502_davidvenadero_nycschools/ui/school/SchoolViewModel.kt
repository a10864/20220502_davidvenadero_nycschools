package com.kapital.a20220502_davidvenadero_nycschools.ui.school

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.kapital.a20220502_davidvenadero_nycschools.data.entities.School
import com.kapital.a20220502_davidvenadero_nycschools.data.remote.SchoolRemote.Companion.toSchool
import com.kapital.a20220502_davidvenadero_nycschools.data.repository.DefaultSchoolRepository
import com.kapital.a20220502_davidvenadero_nycschools.data.repository.SchoolRepository
import com.kapital.a20220502_davidvenadero_nycschools.utils.Status
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolViewModel @Inject constructor(
    private val defaultSchoolRepository: SchoolRepository
) : ViewModel() {
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
    private val _schools = MutableLiveData<List<School>>()
    val schools: LiveData<List<School>>
        get() = _schools
    private val _error = MutableLiveData<String>()
    val error: LiveData<String>
        get() = _error
    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean>
        get() = _isLoading


    fun getAllSchools() {
        _isLoading.postValue(true)
        viewModelScope.launch(dispatcher) {
            val response = defaultSchoolRepository.getAllSchools()
            if (response.isSuccessful) {
                _schools.postValue(response.body()?.map { it.toSchool() } ?: emptyList())
            } else {
                _error.postValue("Network Error")
            }
            _isLoading.postValue(false)
        }
    }
}