package com.kapital.a20220502_davidvenadero_nycschools.ui.school


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.kapital.a20220502_davidvenadero_nycschools.R
import com.kapital.a20220502_davidvenadero_nycschools.data.entities.School
import com.kapital.a20220502_davidvenadero_nycschools.databinding.FragmentSchoolBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_school.*

@AndroidEntryPoint
class SchoolFragment : Fragment(), SchoolAdapter.SchoolItemListener {
    private lateinit var binding: FragmentSchoolBinding
    private val viewModel: SchoolViewModel by viewModels()
    private lateinit var adapter: SchoolAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentSchoolBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.getAllSchools()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        setupObservers()


    }

    private fun setupRecyclerView() {
        adapter = SchoolAdapter(this)
        binding.rvSchool.layoutManager = LinearLayoutManager(requireContext())
        binding.rvSchool.adapter = adapter
    }

    private fun setupObservers() {
        viewModel.schools.observe(viewLifecycleOwner) {
            adapter.setItems(it)
        }
        viewModel.error.observe(viewLifecycleOwner) {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
        }
        viewModel.isLoading.observe(viewLifecycleOwner) {
            if (it)
                progress_bar_school.visibility = View.VISIBLE
            else
                progress_bar_school.visibility = View.GONE
        }
    }

    override fun onClick(school: School) {
        findNavController().navigate(
            R.id.action_schoolFragment_to_schoolScoresFragment,
            bundleOf("school" to school)
        )
    }


}