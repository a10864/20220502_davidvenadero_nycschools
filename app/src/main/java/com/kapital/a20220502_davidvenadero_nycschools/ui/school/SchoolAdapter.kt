package com.kapital.a20220502_davidvenadero_nycschools.ui.school

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kapital.a20220502_davidvenadero_nycschools.data.entities.School
import com.kapital.a20220502_davidvenadero_nycschools.databinding.ElementSchoolBinding

class SchoolAdapter(private val listener: SchoolItemListener) :
    RecyclerView.Adapter<SchoolViewHolder>() {
    interface SchoolItemListener {
        fun onClick(school: School)
    }

    private val items = mutableListOf<School>()

    fun setItems(items: List<School>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolViewHolder {
        val binding: ElementSchoolBinding =
            ElementSchoolBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SchoolViewHolder(binding, listener)
    }

    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) =
        holder.bind(items[position])

    override fun getItemCount(): Int = items.size
}

class SchoolViewHolder(
    private val itemSchoolBinding: ElementSchoolBinding,
    private val listener: SchoolAdapter.SchoolItemListener
) : RecyclerView.ViewHolder(itemSchoolBinding.root) {
    private lateinit var school: School
    fun bind(item: School) {
        this.school = item
        itemSchoolBinding.txtSchoolName.text = item.name
        itemSchoolBinding.txtDescription.text = item.description
        itemSchoolBinding.txtLocation.text = item.location
        itemSchoolBinding.btnAction.setOnClickListener {
            listener.onClick(school)
        }

    }

}