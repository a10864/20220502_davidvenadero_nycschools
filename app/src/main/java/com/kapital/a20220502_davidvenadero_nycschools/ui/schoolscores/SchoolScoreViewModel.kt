package com.kapital.a20220502_davidvenadero_nycschools.ui.schoolscores

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.kapital.a20220502_davidvenadero_nycschools.data.entities.School
import com.kapital.a20220502_davidvenadero_nycschools.data.entities.SchoolSat
import com.kapital.a20220502_davidvenadero_nycschools.data.remote.SchoolSatRemote.Companion.toSchoolSat
import com.kapital.a20220502_davidvenadero_nycschools.data.repository.DefaultSchoolRepository
import com.kapital.a20220502_davidvenadero_nycschools.data.repository.SchoolRepository
import com.kapital.a20220502_davidvenadero_nycschools.utils.Status
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolScoreViewModel @Inject constructor(
    private val defaultSchoolRepository: SchoolRepository
) : ViewModel() {
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
    private val _schoolScore = MutableLiveData<SchoolSat>()
    val schoolScore: LiveData<SchoolSat>
        get() = _schoolScore
    private val _error = MutableLiveData<String>()
    val error: LiveData<String>
        get() = _error
    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean>
        get() = _isLoading

    fun getAllSchoolsScore(school: School) {
        _isLoading.postValue(true)
        viewModelScope.launch(dispatcher) {
            val response = defaultSchoolRepository.getAllSchoolScore()
            if (response.isSuccessful) {
                _schoolScore.postValue(
                    response.body()?.find { it.dbn == school.dbn }?.toSchoolSat() ?: SchoolSat()
                )
            } else {
                _error.postValue("Network Error")
            }
            _isLoading.postValue(false)
        }
    }
}