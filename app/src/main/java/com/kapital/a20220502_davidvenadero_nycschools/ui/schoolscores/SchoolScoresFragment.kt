package com.kapital.a20220502_davidvenadero_nycschools.ui.schoolscores

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.kapital.a20220502_davidvenadero_nycschools.R
import com.kapital.a20220502_davidvenadero_nycschools.data.entities.School
import com.kapital.a20220502_davidvenadero_nycschools.databinding.FragmentSchoolScoresBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_school_scores.*

@AndroidEntryPoint
class SchoolScoresFragment : Fragment() {
    private lateinit var binding: FragmentSchoolScoresBinding
    private val viewModel: SchoolScoreViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentSchoolScoresBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObservers()
        arguments?.getParcelable<School>("school")?.let {
            setupUi(it)
            viewModel.getAllSchoolsScore(it)
        }
    }

    private fun setupUi(school: School) {
        binding.txtDescriptionSchoolDetail.text = school.description
        binding.txtName.text = school.name
    }

    private fun setupObservers() {
        viewModel.schoolScore.observe(viewLifecycleOwner, Observer {
            binding.txtTitleEdit.text = it.schoolName
            binding.txtDescriptionDetail.text = requireContext().getString(
                R.string.detail,
                it.numSat,
                it.scoreMat,
                it.scoreReading,
                it.scoreWrite
            )
        })
        viewModel.error.observe(viewLifecycleOwner) {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
        }
        viewModel.isLoading.observe(viewLifecycleOwner) {
            if (it)
                progress_bar_score.visibility = View.VISIBLE
            else
                progress_bar_score.visibility = View.GONE
        }
    }
}