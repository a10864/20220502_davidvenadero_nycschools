package com.kapital.a20220502_davidvenadero_nycschools.utils

enum class Status {
    SUCCESS,
    ERROR
}