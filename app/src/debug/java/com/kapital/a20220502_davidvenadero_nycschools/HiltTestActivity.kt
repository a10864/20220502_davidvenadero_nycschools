package com.kapital.a20220502_davidvenadero_nycschools

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HiltTestActivity:AppCompatActivity()