package com.kapital.a20220502_davidvenadero_nycschools.repository

import com.kapital.a20220502_davidvenadero_nycschools.data.remote.SchoolRemote
import com.kapital.a20220502_davidvenadero_nycschools.data.remote.SchoolSatRemote
import com.kapital.a20220502_davidvenadero_nycschools.data.remote.SchoolService
import com.kapital.a20220502_davidvenadero_nycschools.data.repository.DefaultSchoolRepository
import com.kapital.a20220502_davidvenadero_nycschools.data.repository.SchoolRepository
import com.kapital.a20220502_davidvenadero_nycschools.utils.BaseUnitTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.internal.verification.VerificationModeFactory.times
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import retrofit2.Response

@ExperimentalCoroutinesApi
class SchoolRepositoryTest : BaseUnitTest() {


    private lateinit var schoolRepository: SchoolRepository
    private val service:SchoolService=mock()
    private val schoolRemote:Response<List<SchoolRemote>>  = mock()
    private val schoolSatRemote: Response<List<SchoolSatRemote>> = mock()
    @Before
    fun setup() {
        schoolRepository=DefaultSchoolRepository(service)
    }

    @Test
    fun `verify call the service by repository`()= runBlockingTest {
        schoolRepository.getAllSchools()
        verify(service,times(1)).getAllSchools()
    }

    @Test
    fun `get all school items`()= runBlockingTest {
        whenever(service.getAllSchools()).thenReturn(schoolRemote)
        assertEquals(schoolRemote,schoolRepository.getAllSchools())
    }

    @Test
    fun `get all school score items`()= runBlockingTest {
        whenever(service.getAllSchoolScores()).thenReturn(schoolSatRemote)
        assertEquals(schoolSatRemote,schoolRepository.getAllSchoolScore())
    }
}