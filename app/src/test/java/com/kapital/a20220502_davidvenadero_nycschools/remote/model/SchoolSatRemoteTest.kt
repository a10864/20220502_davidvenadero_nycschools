package com.kapital.a20220502_davidvenadero_nycschools.remote.model

import com.google.common.truth.Truth.assertThat
import com.kapital.a20220502_davidvenadero_nycschools.data.remote.SchoolSatRemote
import com.kapital.a20220502_davidvenadero_nycschools.data.remote.SchoolSatRemote.Companion.toSchoolSat
import org.junit.Test

class SchoolSatRemoteTest {
    @Test
    fun `School Sat Remote to School Sat`(){
        val schoolSatRemote=SchoolSatRemote(
            dbn= "01M292",
        school_name= "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES",
        num_of_sat_test_takers= "29",
        sat_critical_reading_avg_score= "355",
        sat_math_avg_score= "404",
        sat_writing_avg_score= "363"
        )
        val schoolSat=schoolSatRemote.toSchoolSat()
        assertThat(schoolSatRemote.dbn).isEqualTo(schoolSat.dbn)
    }
}